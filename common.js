let numb1 = prompt("Please, enter a number.");

while (isNaN(+numb1) || numb1 === "" || numb1 === null) {
    numb1 = prompt("Not a number. Please, enter a number.", numb1);
}

let numb2 = prompt("Please, enter one more number.");

while (isNaN(+numb2) || numb2 === "" || numb2 === null) {
    numb2 = prompt("Not a number. Please, enter a number.", numb2);
}

let operator = prompt("Please choose an operation by entering the operator: + or - or * or /");

while (operator !== "+" && operator !== "-" && operator !== "*" && operator !== "/") {
    operator = prompt("Incorrect operator. Possible operators: + or - or * or /");
}

function operation(arg1, arg2, symb) {
    let result;

    switch (symb) {
        case "+":
            result = +arg1 + +arg2;
            break;
        case "-":
            result = arg1 - arg2;
            break;
        case "*":
            result = arg1 * arg2;
            break;
        case "/":
            result = arg1 / arg2;
            break;
        // default:  `in case we dont have validation of operator input
        //     console.log("Incorrect operator."); 
        //     return;
    }
    console.log("Result:" + result);
    // console.log(typeof result);
}

operation(numb1, numb2, operator);
